<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			SEBLOD 1.x (CCK for Joomla!)
**/

// No Direct Access
defined('_JEXEC') or die('Restricted access');
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod  =  clone $this;

if (array_key_exists('cckitems', get_object_vars($jSeblod))) {
  $cckitems  =  $jSeblod->cckitems;
} else {
  return true;
}


$data = array();
$data['id'] = $this->content->id;
$data['cattitle'] = $this->content->category;
$data['created'] = $this->content->created;
$data['modified'] = $this->content->modified;

print_r($cckitems);die();
for ($i = 0, $n = count($cckitems); $i < $n; $i++) {

  $item  =  $cckitems[$i];

  if (is_array($item)) {
    foreach ($item as $itm) {
      $data[$jSeblod->$item->name][] = $itm;
    }
  } else {
    $data[$jSeblod->$item->name] = $jSeblod->$item->value;
  }
}

echo json_encode((object)$data);

?>