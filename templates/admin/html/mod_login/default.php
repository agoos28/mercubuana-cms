<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
$return = base64_encode(JURI::base());
?>
<?php if($type == 'logout') : ?>
<form action="index.php" method="post" name="form-login" id="form-login">
  <div class="user-profile border-top padding-horizontal-10 block">
      <div class="inline-block">
      <a href="/administration/user?view=user&layout=customform&userid=<?php echo $user->get('id'); ?>" />
          <img style="width: 50px;height: auto;" src="<?php echo $user->get('propict'); ?>" alt="">
          </a>
      </div>
      <div class="inline-block">
          <h5 class="no-margin"> Welcome </h5>
          <h4 class="no-margin"> 
          <a href="/administration/user?view=user&layout=customform&userid=<?php echo $user->get('id'); ?>" />
          <?php echo $user->get('name'); ?> 
          </a>
          </h4>
          <a href="<?php echo JRoute::_('index.php?option=com_user').'?task=logout&amp;return='.$return; ?>" class="btn user-options sb_toggle" type="submit" title="Logout">
              <i class="fa fa-power-off"></i>
          </a>
      </div>
  </div>
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="logout" />
	<input type="hidden" name="return" value="<?php echo $return; ?>" />
</form>
<?php endif; ?>
