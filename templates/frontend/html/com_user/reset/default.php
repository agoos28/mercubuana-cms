<?php // @version $Id: default.php  $
defined('_JEXEC') or die('Restricted access');
?>
<?php // @version $Id: default.php$
defined('_JEXEC') or die('Restricted access');
?>

<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">

        <div class="wrapper-page">

          <div class="account-pages">
            <div class="account-box">
              <div class="text-center account-logo-box">
                <h2 class="text-uppercase">
                  <a href="index.html" class="text-success">
                    <span><img src="<?php echo JURI::base(); ?>templates/frontend/images/logo_dark.png" alt="" height="30"></span>
                  </a>
                </h2>
                <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
              </div>
              <div class="account-content">
                <div class="text-center m-b-20">
                  <p class="text-muted m-b-0">Enter your email address and we'll send you an email with instructions to reset your password. </p>
                </div>
                <form class="form-horizontal" action="index.php?option=com_user&task=requestreset" method="post" >

                  <div class="form-group row m-b-20">
                    <div class="col-12">
                      <label for="emailaddress">Email address</label>
                      <input class="form-control" type="email" id="emailaddress" required="" placeholder="nama@email.com" name="email">
                    </div>
                  </div>

                  <div class="form-group row text-center m-t-10">
                    <div class="col-12">
                      <button class="btn btn-md btn-block btn-primary waves-effect waves-light" type="submit">Reset Password</button>
                    </div>
                  </div>

                </form>

                <div class="clearfix"></div>

                <div class="row m-t-40">
                  <div class="col-sm-12 text-center">
                    <p class="text-muted">Back to <a href="index.html" class="text-dark m-l-5"><b>Log In</b></a></p>
                  </div>
                </div>

              </div>

            </div>
            <!-- end card-box-->
          </div>


        </div>
        <!-- end wrapper -->

      </div>
    </div>
  </div>
</section>

<h1 class="componentheading"><?php echo JText::_('FORGOT_YOUR_PASSWORD'); ?></h1>

<form action="index.php?option=com_user&amp;task=requestreset" method="post" class="josForm form-validate">
	<p><?php echo JText::_('RESET_PASSWORD_REQUEST_DESCRIPTION'); ?></p>

	<label for="email" class="hasTip" title="<?php echo JText::_('RESET_PASSWORD_EMAIL_TIP_TITLE'); ?>::<?php echo JText::_('RESET_PASSWORD_EMAIL_TIP_TEXT'); ?>"><?php echo JText::_('Email Address'); ?>:</label>
	<input id="email" name="email" type="text" class="required validate-email" />

	<button type="submit" class="validate"><?php echo JText::_('Submit'); ?></button>
	<?php echo JHTML::_( 'form.token' ); ?>
</form>