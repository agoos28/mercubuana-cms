<?php // @version $Id$
defined( '_JEXEC' ) or die( 'Restricted access' );
global $mainframe;
$user	= JFactory::getUser();
if($user->id){
$mainframe->redirect($url, JText::_('You must login first') );
}

echo $this->loadTemplate( $this->type );

