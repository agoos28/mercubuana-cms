<?php // @version $Id: default_login.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
?>
<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="wrapper-page">
          <div class="account-pages">
            <div class="account-box">
              <div class="account-logo-box">
                <h2 class="text-uppercase text-center">
                  <a href="index.html" class="text-success">
                    <span><img src="<?php echo JURI::base(); ?>templates/frontend/images/logo_dark.png" alt="" height="30"></span>
                  </a>
                </h2>
                <h5 class="text-uppercase font-bold m-b-5 m-t-50">Log In</h5>
                <p class="m-b-0">Login to your Account</p>
              </div>
              <div class="account-content">
                <form class="form-horizontal" action="<?php echo JRoute::_('index.php', true, $this->params->get('usesecure')); ?>" method="post" name="login">

                  <div class="form-group m-b-20 row">
                    <div class="col-12">
                      <label for="emailaddress">Email address</label>
                      <input name="username" class="form-control" type="email" id="emailaddress" required="" placeholder="name@email.com">
                    </div>
                  </div>

                  <div class="form-group row m-b-20">
                    <div class="col-12">
                      <a href="<?php echo JRoute::_('index.php?option=com_user&view=reset'); ?>" class="text-muted pull-right"><small>Forgot your password?</small></a>
                      <label for="password">Password</label>
                      <input name="passwd" class="form-control" type="password" required="" id="password" placeholder="Enter your password">
                    </div>
                  </div>

                  <div class="form-group row text-center m-t-10">
                    <div class="col-12">
                      <button type="submit" name="submit" class="btn btn-md btn-block btn-primary waves-effect waves-light" type="submit">Log In</button>
                    </div>
                  </div>
                  <noscript><?php echo JText::_('WARNJAVASCRIPT'); ?></noscript>
                  <input type="hidden" name="option" value="com_user" />
                  <input type="hidden" name="task" value="login" />
                  <input type="hidden" name="return" value="<?php echo base64_encode(JURI::current()); ?>" />
                  <?php echo JHTML::_('form.token'); ?>
                </form>

                <div class="row m-t-50">
                  <div class="col-sm-12 text-center">
                    <p class="text-muted">Don't have an account? <a href="<?php echo JRoute::_('index.php?option=com_user&task=register'); ?>" class="text-dark m-l-5"><b>Register</b></a></p>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <!-- end card-box-->


        </div>
        <!-- end wrapper -->

      </div>
    </div>
  </div>
</section>