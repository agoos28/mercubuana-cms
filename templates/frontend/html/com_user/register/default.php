<?php // @version $Id: default.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
?>

<script type="text/javascript">
  Window.onDomReady(function() {
    document.formvalidator.setHandler('passverify', function(value) {
      return ($('password').value == value);
    });
  });
</script>

<div class="container">
  <div class="row">
    <div class="col-sm-12">

      <div class="wrapper-page">

        <div class="account-pages">
          <div class="account-box">
            <div class="account-logo-box">
              <h2 class="text-uppercase text-center">
                <a href="index.html" class="text-success">
                  <span><img src="<?php echo JURI::base(); ?>templates/frontend/images/logo_dark.png" alt="" height="30"></span>
                </a>
              </h2>
              <h5 class="text-uppercase font-bold m-b-5 m-t-50">Register</h5>
              <p class="m-b-0">Get access to your UMB Alumni Account</p>
            </div>
            <div class="account-content">
              <form class="form-horizontal" action="/register" method="post">

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="username">Full Name</label>
                    <input class="form-control" type="text" id="username" required="" placeholder="Nama Lengkap" name="name">
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="username">NIM</label>
                    <input class="form-control" type="text" id="username" required="" placeholder="Nama Induk Mahasiswa" name="npm">
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="emailaddress">Email address</label>
                    <input class="form-control" type="email" id="emailaddress" required="" placeholder="nama@email.com" name="email">
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" required="" id="password" placeholder="Enter your password" name="password">
                  </div>
                </div>
                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="password">Confirm Password</label>
                    <input class="form-control" type="password" required="" id="password" placeholder="Enter your password" name="password2">
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="emailaddress">Mobile Phone</label>
                    <input class="form-control" type="text" id="text" required="" placeholder="+62-123123123" name="phone">
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="emailaddress">Date of Birth</label>
                    <input class="form-control" type="date" name="birthdate">
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="emailaddress">Gender</label>
                    <div class="col-10">
                      <select class="form-control" name="gender">
                        <option>Pria</option>
                        <option>Wanita</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="emailaddress">Jurusan</label>
                    <div class="col-10">
                      <select class="form-control" name="study">
                        <option>Manajemen</option>
                        <option>Akunttansi</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="emailaddress">Jenjang</label>
                    <div class="col-10">
                      <select class="form-control" name="degree">
                        <option>D3</option>
                        <option>S1</option>
                        <option>S2</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">
                    <label for="emailaddress">Member of Class</label>
                    <div class="col-10">
                      <select class="form-control" name="class">
                        <option>2019</option>
                        <option>2018</option>
                        <option>2017</option>
                        <option>2016</option>
                        <option>2015</option>
                        <option>2014</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group row m-b-20">
                  <div class="col-12">

                    <div class="checkbox checkbox-success">
                      <input id="remember" type="checkbox" checked="">
                      <label for="remember">
                        I accept <a href="#">Terms and Conditions</a>
                      </label>
                    </div>

                  </div>
                </div>

                <div class="form-group row text-center m-t-10">
                  <div class="col-12">
                    <button class="btn btn-md btn-block btn-primary waves-effect waves-light" type="submit">Resgister</button>
                  </div>
                </div>
                <input type="hidden" name="task" value="register_save" />
                <input type="hidden" name="id" value="0" />
                <input type="hidden" name="gid" value="0" />
                <?php echo JHTML::_('form.token'); ?>
              </form>

              <div class="row m-t-50">
                <div class="col-12 text-center">
                  <p class="text-muted">Already have an account? <a href="<?php echo JURI::base(); ?>login" class="text-dark m-l-5"><b>Log In</b></a></p>
                </div>
              </div>

            </div>
          </div>
          <!-- end card-box-->
        </div>


      </div>
      <!-- end wrapper -->

    </div>
  </div>
</div>
