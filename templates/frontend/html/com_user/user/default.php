<?php // @version $Id: default.php  $
defined('_JEXEC') or die('Restricted access');
$user	= JFactory::getUser();

$db = &JFactory::getDBO();
$query = "SELECT * FROM #__user_experience WHERE user_id = ". $user->id . " ORDER BY start_date DESC";
$db->setQuery($query);
$user->experience = $db->loadObjectList();

//print_r($user);
?>
<div class="wrapper">
  <div class="container">

    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <div class="page-title-box">
          <div class="btn-group pull-right">
          </div>
          <h4 class="page-title">Profile</h4>
        </div>
      </div>
    </div>
    <!-- end page title end breadcrumb -->


    <div class="row">
      <div class="col-sm-12">
        <div class="profile-bg-picture" style="background-image:url('<?php echo JURI::base(); ?>templates/frontend/images/bg-profile.jpg')">
          <span class="picture-bg-overlay"></span><!-- overlay -->
        </div>
        <!-- meta -->
        <div class="profile-user-box">
          <div class="row">
            <div class="col-sm-6">
              <span class="pull-left m-r-15"><img src="<?php echo JURI::base().$user->propict; ?>" alt="" class="thumb-lg rounded-circle"></span>
              <div class="media-body">
                <h4 class="m-t-5 m-b-5 font-18 ellipsis"><?php echo $user->name; ?></h4>
                <p class="font-13"> <?php echo $user->study; ?> <?php echo $user->class; ?></p>
                <p class="text-muted m-b-0"><small><?php echo $user->hometown; ?></small></p>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="text-right">
                <button type="button" class="btn btn-success waves-effect waves-light">
                  <i class="mdi mdi-account-settings-variant m-r-5"></i> Edit Profile
                </button>
              </div>
            </div>
          </div>
        </div>
        <!--/ meta -->
      </div>
    </div>
    <!-- end row -->

    <div class="row">
      <div class="col-md-4">
        <!-- Personal-Information -->
        <div class="card-box">
          <h4 class="header-title mt-0 m-b-20">Personal Information</h4>
          <div class="panel-body">
            <p class="text-muted font-13">
              <?php echo $user->bio; ?>
            </p>

            <hr />

            <div class="text-left">
              <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15"><?php echo $user->name; ?></span></p>
              <p class="text-muted font-13"><strong>NPM :</strong> <span class="m-l-15"><?php echo $user->npm; ?></span></p>
              <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15"><?php echo $user->phone; ?></span></p>
              <p class="text-muted font-13"><strong>Date Birth :</strong> <span class="m-l-15"><?php echo $user->birthdate; ?></span></p>
              <p class="text-muted font-13"><strong>Gender :</strong> <span class="m-l-15"><?php echo $user->gender; ?></span></p>
              <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $user->email; ?></span></p>
              <p class="text-muted font-13"><strong>Job :</strong> <span class="m-l-15"><?php echo $user->job; ?></span></p>
              <p class="text-muted font-13"><strong>Field of Study :</strong> <span class="m-l-15"><?php echo $user->study; ?></span></p>
              <p class="text-muted font-13"><strong>Degree :</strong> <span class="m-l-15"><?php echo $user->degree; ?></span></p>
              <p class="text-muted font-13"><strong>Member of Class :</strong> <span class="m-l-15"><?php echo $user->class; ?></span></p>
              <p class="text-muted font-13"><strong>Hometown :</strong> <span class="m-l-15"><?php echo $user->hometown; ?></span></p>

            </div>

          </div>
        </div>
        <!-- Personal-Information -->


      </div>


      <div class="col-md-8">
        <div class="card-box">
          <h4 class="header-title mt-0 m-b-20">Experience</h4>
        <?php 
          $count = count($user->experience);
          for($i=0;$i<$count;$i++){ 
          $item = $user->experience[$i];
          ?>
        
          <div class="">
            <div class="">
              <h5 class="text-custom m-b-5"><?php echo $item->position; ?></h5>
              <p class="m-b-0"><?php echo $item->company_name; ?></p>
              <p><b><?php echo $item->start_date; ?> - <?php echo ($item->start_date != '0000-00-00') ? $item->start_date : 'now'; ?></b></p>

              <p class="text-muted font-13 m-b-0"><?php echo $item->job_description; ?></p>
            </div>
          <?php 
              if($i != $count - 1){
                echo '<hr>';
              }
            } 
          ?>

          </div>
        </div>


      </div>
      <!-- end col -->

    </div>
    <!-- end row -->

  </div> <!-- end container -->
</div>