<?php // @version $Id: blog_item.php 9718 2007-12-20 22:35:36Z eddieajau $
defined('_JEXEC') or die('Restricted access');

?>

<?php echo $this->item->event->beforeDisplayContent; ?>

<?php $item = json_decode(JFilterOutput::ampReplace($this->item->text)); ?>

<div class="col-md-6 col-sm-6 col-xs-12">
  <div class="card m-b-20">
    <img class="card-img-top img-fluid" src="<?php echo $item->content_image; ?>" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title"><?php echo $item->content_title; ?></h4>
      <p class="card-text">
        <small class="text-muted">Last updated <?php echo JHTML::_('date', $item->modified); ?></small>
      </p>
    </div>
  </div>
</div>
<?php //echo JFilterOutput::ampReplace($this->item->text);  ?>

<?php echo $this->item->event->afterDisplayContent;
