<?php
defined('_JEXEC') or die('Restricted access');
$curentUrl = JURI::getInstance();
$baseUrl = JURI::base();
$tmplUrl = $baseUrl. 'templates/frontend/'; 
$conf   =& JFactory::getConfig();
$sitename   = $conf->getValue('config.sitename');
$user	= JFactory::getUser();
$headerstuff=$this->getHeadData();
$menu = & JSite::getMenu();
$navigationParam = $menu->getActive();
reset($headerstuff['styleSheets']);
foreach($headerstuff['styleSheets'] as $key=>$value){
	unset($headerstuff['styleSheets'][$key]);
}
reset($headerstuff['scripts']);
foreach($headerstuff['scripts'] as $key=>$value){
	unset($headerstuff['scripts'][$key]);
}
reset($headerstuff['script']);
foreach($headerstuff['script'] as $key=>$value){
	unset($headerstuff['script'][$key]);
}		
$this->setHeadData($headerstuff);
$this->setGenerator('agoos28');

$ishome = false;
if ($navigationParam == $menu->getDefault()){
	$ishome = true;
	$jquery = 'jquery.112.js?v=3';
}else{
	$jquery = 'jquery.js?v=3';
}
$buffer = $this->getBuffer();
$message = JApplication::getMessageQueue();
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>UMB CTC - UMB Career and Training Center</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Universitas Mercubuana Career and Training Center" name="description" />
        <meta content="DMT" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="<?php echo $tmplUrl; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $tmplUrl; ?>css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $tmplUrl; ?>css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $tmplUrl; ?>css/style.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo $tmplUrl; ?>js/jquery.min.js"></script>
        <script src="<?php echo $tmplUrl; ?>js/modernizr.min.js"></script>

    </head>
<body class="<?php echo $navigationParam->alias; ?>_page">
  <?php 
  if($message){
    $message;
  }
  if($user->id){ ?>
  <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!--<a href="index.html" class="logo">-->
                        <!--Adminox-->
                        <!--</a>-->
                        <!-- Image Logo -->
                        <a href="home.html" class="logo">
                            <img src="<?php echo $tmplUrl; ?>images/logo_dark.png" alt="" height="24" class="logo-lg">
                            <img src="<?php echo $tmplUrl; ?>images/logo_sm.png" alt="" height="24" class="logo-sm">
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-inline float-right mb-0">

                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="dripicons-bell noti-icon"></i>
                                    <span class="badge badge-pink noti-icon-badge">4</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5><span class="badge badge-danger float-right">5</span>Notification</h5>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                                        <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1 min ago</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                                        <p class="notify-details">New user registered.<small class="text-muted">1 min ago</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                                        <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1 min ago</small></p>
                                    </a>

                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                                        View All
                                    </a>

                                </div>
                            </li>

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="<?php echo $baseUrl.$user->propict; ?>" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5 class="text-overflow"><small>Welcome ! <?php echo $user->name; ?></small> </h5>
                                    </div>

                                    <!-- item-->
                                    <a href="<?php echo $baseUrl; ?>user" class="dropdown-item notify-item">
                                        <i class="zmdi zmdi-account-circle"></i> <span>Profile</span>
                                    </a>

                                    <!-- item-->
                                    <a href="<?php echo $baseUrl; ?>user?task=logout" class="dropdown-item notify-item">
                                        <i class="zmdi zmdi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a href="<?php echo $baseUrl; ?>"><i class="fa fa-home"></i>Home</a>
                            </li>

                            <li class="has-submenu">
                                <a href="<?php echo $baseUrl; ?>people"><i class="fa fa-users"></i>People</a>
                            </li>

                            <li class="has-submenu">
                                <a href="<?php echo $baseUrl; ?>"><i class="fa fa-briefcase"></i>Companies</a>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->
  <?php } 


  ?>

  <jdoc:include type="component" />


  
  <script src="<?php echo $tmplUrl; ?>js/tether.min.js"></script><!-- Tether for Bootstrap -->
  <script src="<?php echo $tmplUrl; ?>js/bootstrap.min.js"></script>
  <script src="<?php echo $tmplUrl; ?>js/waves.js"></script>
  <script src="<?php echo $tmplUrl; ?>js/jquery.slimscroll.js"></script>
  <script src="<?php echo $tmplUrl; ?>js/jquery.scrollTo.min.js"></script>

  <!-- App js -->
  <script src="<?php echo $tmplUrl; ?>js/jquery.core.js"></script>
  <script src="<?php echo $tmplUrl; ?>js/jquery.app.js"></script>

</body>
