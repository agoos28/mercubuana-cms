<?php
/* Copyright (C) 2012 SEBLOD. All Rights Reserved. */

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php
JModel::addIncludePath (JPATH_SITE . DS . 'components' . DS . 'com_cckjseblod' . DS . 'models');
$cart_model =& JModel::getInstance('cart', 'CCKjSeblodModel');
$model = $this->getmodel();
if(JRequest::getVar('userid')){
  $user = $model->getuser(JRequest::getVar('userid'));
  $experience = $model->userExperience(JRequest::getVar('userid'));
  $friends = $model->userFriends(JRequest::getVar('userid'));
}
?>
<div class="row">
  <div class="col-sm-12">
    <div class="tabbable">
      <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
        <li class="active">
          <a data-toggle="tab" href="#panel_overview">
            Overview
          </a>
        </li>
        <li>
          <a data-toggle="tab" href="#panel_edit_account">
            Edit Account
          </a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="panel_overview" class="tab-pane fade in active">
          <div class="row">
            <div class="col-sm-5 col-md-4">
              <div class="user-left" style="border: none;">
                <div class="space20"></div>
                <div class="center">
                  <img data-src="<?php echo $user->propict ?>" class="img-circle" style="width: 140px; height: 140px;"
                    src="<?php echo $user->propict ?>">
                  <h4><strong>
                      <?php echo $user->name ?></strong></h4>
                  <hr>
                  <h4><strong>
                      NIM : <?php echo $user->npm ?></strong></h4>
                  <hr>
                </div>
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th colspan="3">User Information</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Study</td>
                      <td>
                        <?php echo $user->study ?> -
                        <?php echo $user->class ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>Hometown:</td>
                      <td>
                        <?php echo $user->hometown ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>email:</td>
                      <td>
                        <a href="">
                          <?php echo $user->email ?>
                        </a></td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>phone:</td>
                      <td>
                        <?php echo $user->phone ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>

                  </tbody>
                </table>
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th colspan="3">General information</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Bio:</td>
                      <td>
                        <?php echo $user->bio ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>Occupation:</td>
                      <td>
                        <?php echo $user->job ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th colspan="3">Additional information</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>User Type</td>
                      <td>
                        <?php echo $user->usertype ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>Registered</td>
                      <td>
                        <?php echo $user->registerDate ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>Last Logged In</td>
                      <td>
                        <?php echo $user->lastvisitDate ?>
                      </td>
                      <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-sm-7 col-md-8">
              <div class="space20 pull-right">
                <a class="btn btn-light-grey" onclick="javascript: history.go(-1); return false;" href="#"><i
                    class="fa fa-arrow-circle-left"></i> Back</a>
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">
                    <i class="fa fa-edit"></i>
                    Admin Actions
                  </button>
                  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li>
                      <a href="#panel_edit_account" class="show-tab">
                        <i class="fa fa-pencil"></i> Edit
                      </a>
                    </li>
                    <li>
                      <a href="#panel_edit_account" class="show-tab">
                        <i class="fa fa-pencil"></i> Enable
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-ban"></i> Disable
                      </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                      <a href="#">
                        <i class="fa fa-trash-o"></i> Delete
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="panel panel-white space20" style="box-shadow: 0 0px 2px #c3c3c3">
                <div class="panel-body no-padding">
                  <div class="tabbable no-margin no-padding partition-gray">
                    <ul class="nav nav-tabs" id="myTab">
                      <li class="active">
                        <a data-toggle="tab" href="#users_tab_example1">
                          Job Experience
                        </a>
                      </li>
                      <li class="">
                        <a data-toggle="tab" href="#users_tab_example2">
                          <?php echo count((array)$friends); ?> Friends
                        </a>
                      </li>
                    </ul>
                    <div class="tab-content partition-white">
                      <div id="users_tab_example1" class="tab-pane padding-bottom-5 active">
                        <div class="panel-scroll height-230">
                          <table class="table table-striped table-hover">
                            <tbody>
                              <?php if(count($experience)){
                          foreach($experience as $exp){ 
                            if($exp->end_date = "0000-00-00"){
                              $exp->end_date = "Until now";
                            }else{
                               $exp->end_date = JHtml::_('date', $exp->end_date, '%d %B %Y');
                            }  
                          ?>
                              <tr>
                                <td width="160" style="vertical-align:top;">
                                  <span class="text-small block text-light">Company/Position</span>
                                  <span class="text-large"><?php echo $exp->company_name; ?></span><br />
                                  <?php echo $exp->position; ?>
                                </td>
                                <td style="vertical-align:top;">
                                  <div>
                                    <span class="text-small block text-light">Job desc</span>
                                    <?php echo $exp->job_description; ?>
                                  </div>
                                </td>
                                <td style="vertical-align:top;" width="200">
                                  <div>
                                    <span class="text-small block text-light">Date</span>
                                    <?php echo JHtml::_('date', $exp->start_date, '%d %B %Y'); ?> -
                                    <?php echo $exp->end_date; ?>
                                  </div>
                                </td>
                              </tr>
                              <?php } } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div id="users_tab_example2" class="tab-pane padding-bottom-5">
                        <div class="panel-scroll height-230">
                          <table class="table table-striped table-hover">
                            <tbody>
                              <?php if(count($friends)){
                          foreach($friends as $friend){ 
                            if($exp->end_date = "0000-00-00"){
                              $exp->end_date = "Until now";
                            }else{
                               $exp->end_date = JHtml::_('date', $friend->end_date, '%d %B %Y');
                            }  
                          ?>
                              <tr>
                                <td class="center" style="width: 120px;">
                                <div style="min-height: 52px;">
                                  <a  href="<?php echo JURI::current().'?view=user&layout=customform&userid='.$friend->id ?>">
                                  <img style="width: 50px; height: auto;"
                                    src="<?php echo JURI::base().$friend->propict; ?>" class="img-circle"
                                    alt="." />
                                  </a>
                                </div>
                                </td>
                                <td><span class="text-small block text-light"><?php echo $friend->study; ?></span><span
                                    class="text-large"><a  href="<?php echo JURI::current().'?view=user&layout=customform&userid='.$friend->id ?>"><?php echo $friend->name; ?></a></span></td>
                                <td class="center">
                                  <div>
                                    
                                </td>
                              </tr>
                              <?php } } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-white space20" style="box-shadow: 0 0px 2px #c3c3c3">
                <div class="panel-heading border-light">
                  <i class="clip-menu"></i>
                  <h4 class="panel-title">Recent Activities</h4>
                  <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-close" href="#">
                      <i class="fa fa-times"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body panel-scroll height-230">
                  <ul class="activities">
                    <li>
                      <a class="activity" href="javascript:void(0)">
                        <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-blue"></i> <i
                            class="fa fa-code fa-stack-1x fa-inverse"></i>
                        </span> <span class="desc">You uploaded a new release.</span>
                        <div class="time">
                          <i class="fa fa-clock-o"></i>
                          2 hours ago
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="activity" href="javascript:void(0)">
                        <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-orange"></i> <i
                            class="fa fa-database fa-stack-1x fa-inverse"></i>
                        </span> <span class="desc">DataBase Migration.</span>
                        <div class="time">
                          <i class="fa fa-clock-o"></i>
                          5 hours ago
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="activity" href="javascript:void(0)">
                        <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-yellow"></i> <i
                            class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
                        </span> <span class="desc">You added a new event to the calendar.</span>
                        <div class="time">
                          <i class="fa fa-clock-o"></i>
                          8 hours ago
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="activity" href="javascript:void(0)">
                        <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-green"></i> <i
                            class="fa fa-file-image-o fa-stack-1x fa-inverse"></i>
                        </span> <span class="desc">Kenneth Ross uploaded new images.</span>
                        <div class="time">
                          <i class="fa fa-clock-o"></i>
                          9 hours ago
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="activity" href="javascript:void(0)">
                        <span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-green"></i> <i
                            class="fa fa-file-image-o fa-stack-1x fa-inverse"></i>
                        </span> <span class="desc">Peter Clark uploaded a new image.</span>
                        <div class="time">
                          <i class="fa fa-clock-o"></i>
                          12 hours ago
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="panel_edit_account" class="tab-pane fade">
          <form id="default_action_user" name="default_action_user" method="post"
            action="/index.php?option=com_cckjseblod&view=user&layout=customform" enctype="multipart/form-data"
            target="_self">
            <div class="row">
              <div class="col-md-12">
                <h3>Main Info</h3>
                <hr>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="username">Name</label>
                  <input type="text" value="<?php echo $user->name ?>" size="32" maxlength="50" name="username"
                    id="username" class="form-control">
                </div>
                <div class="form-group">
                  <label for="email">E-mail</label>
                  <input type="text" value="<?php echo $user->email ?>" size="32" maxlength="50" name="email" id="email"
                    class="form-control">
                </div>
                <div class="form-group">
                  <label for="phone">Phone</label>
                  <input type="text" value="<?php echo $user->phone ?>" size="32" maxlength="50" name="phone" id="phone"
                    class="form-control">
                </div>
                <div class="form-group">
                  <label for="usertype">User Level</label>
                  <select size="1" class="form-control" id="usertype" name="usertype">
                    <option value="Registered" <?php if ($user->usertype == 'Registered') {
                      echo 'selected="selected"';
                      }
                      ?>>Member</option>
                    <option <?php if ($user->usertype == 'Super Administrator') {
                      echo 'selected="selected"';
                      } ?> value="Super Administrator">Super Administrator</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>
                    Image Upload
                  </label>
                  <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="fileupload-new thumbnail" style="width: 108px; border-radius: 50px;"><img
                        class="img-circle" src="<?php echo $user->propict ?>" alt="">
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail"></div>
                    <div class="user-edit-image-buttons">
                      <span class="btn btn-azure btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i>
                          Select image</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Change</span>
                        <input type="file">
                      </span>
                      <a href="#" class="btn fileupload-exists btn-red" data-dismiss="fileupload">
                        <i class="fa fa-times"></i> Remove
                      </a>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">
                    Password
                  </label>
                  <input type="password" placeholder="password" class="form-control" name="password" id="password">
                </div>
                <div class="form-group">
                  <label class="control-label">
                    Confirm Password
                  </label>
                  <input type="password" placeholder="password" class="form-control" id="password_again"
                    name="password_again">
                </div>
              </div>

              <div class="col-md-12">
                <h3>Additional Info</h3>
                <hr>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">
                    Twitter
                  </label>
                  <span class="input-icon">
                    <input class="form-control" type="text" placeholder="Text Field">
                    <i class="fa fa-twitter"></i> </span>
                </div>
                <div class="form-group">
                  <label class="control-label">
                    Facebook
                  </label>
                  <span class="input-icon">
                    <input class="form-control" type="text" placeholder="Text Field">
                    <i class="fa fa-facebook"></i> </span>
                </div>
                <div class="form-group">
                  <label class="control-label">
                    Google Plus
                  </label>
                  <span class="input-icon">
                    <input class="form-control" type="text" placeholder="Text Field">
                    <i class="fa fa-google-plus"></i> </span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">
                    Github
                  </label>
                  <span class="input-icon">
                    <input class="form-control" type="text" placeholder="Text Field">
                    <i class="fa fa-github"></i> </span>
                </div>
                <div class="form-group">
                  <label class="control-label">
                    Linkedin
                  </label>
                  <span class="input-icon">
                    <input class="form-control" type="text" placeholder="Text Field">
                    <i class="fa fa-linkedin"></i> </span>
                </div>
                <div class="form-group">
                  <label class="control-label">
                    Skype
                  </label>
                  <span class="input-icon">
                    <input class="form-control" type="text" placeholder="Text Field">
                    <i class="fa fa-skype"></i> </span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div>
                  Required Fields
                  <hr>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8">
                <p>
                  By clicking UPDATE, you are agreeing to the Policy and Terms &amp; Conditions.
                </p>
              </div>
              <div class="col-md-4">
                <div class="form-group margin-bottom-10 text-right">
                  <button style="" name="button_back" href="#panel_overview" type="button"
                    class="btn btn-default show-tab">Cancel</button>
                  <button type="submit" class="btn btn-primary">Save Change</button>
                </div>
              </div>
            </div>
            <input type="hidden" name="usrid" value="<?php echo JRequest::getVar('userid'); ?>" />
            <input type="hidden" name="task" value="saveedit" />
            <input type="hidden" name="return" value="<?php echo $_SERVER['HTTP_REFERER']; ?>" />
            <?php echo JHTML::_('form.token'); ?>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>