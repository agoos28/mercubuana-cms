<?php
/* Copyright (C) 2012 SEBLOD. All Rights Reserved. */

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<style>
.sectiontableentry1, .sectiontableentry2{
	transition: all 0s;
	transform: translate(0,0);
}
.slideup{
	transition: all 0.2s ease;
	transform: translate(0,-100%);
}
.slidedown{
	transition: all 0.2s ease;
	transform: translate(0,100%);
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(e) {
    function hidefirst(){
		jQuery('a.mup:first').addClass('disabled')
		jQuery('a.mup').not(jQuery('a.mup:first')).removeClass('disabled')
		jQuery('a.mdown:last').addClass('disabled')
		jQuery('a.mdown').not(jQuery('a.mdown:last')).removeClass('disabled')
	}
	hidefirst()
	$('body').on('click', 'a.mup', function(e){
		var th = jQuery(this)
		var parent = th.closest('tr')
		var prev = parent.prev()
		var next = parent.next()
		var curvar = parent.find('input.order').val()
		prev.find('input.order').val(parseInt(curvar))
		parent.find('input.order').val(parseInt(curvar) - 1)
		parent.addClass('slideup')
		prev.addClass('slidedown')
		setTimeout(function(){
			parent.removeClass('slideup')
			parent.insertBefore(prev)
			prev.removeClass('slidedown')
			hidefirst()
		}, 200)
		
		//parent.insertBefore(prev)
		//jQuery('.reorder').click()
		e.preventDefault()
	})
	$('body').on('click', 'a.mdown', function(e){
		var th = jQuery(this)
		var parent = th.closest('tr')
		var prev = parent.prev()
		var next = parent.next()
		var curvar = parent.find('input.order').val()
		next.find('input.order').val(parseInt(curvar))
		parent.find('input.order').val(parseInt(curvar) + 1)
		parent.addClass('slidedown')
		next.addClass('slideup')
		setTimeout(function(){
			parent.removeClass('slidedown')
			parent.insertAfter(next)
			next.removeClass('slideup')
			hidefirst()
		}, 200)
		
		//parent.insertAfter(next);
		//jQuery('.reorder').click()
		e.preventDefault()
	})
	$('.datatable').on( 'draw.dt', function () {
    hidefirst()
} );
	$('#adminForm').on('click', '.publishing', function(e){
		var th = $(this)
		th.text('Wait')
		var cid = th.data('id')
		if(th.data('action') == 'enable'){
			var params = $('#adminForm').serialize()+'&task=article_publish&methode=ajax&cid='+cid
			var classreplace = 'btn btn-xs btn-success tooltips disable publishing'
			var tooltip = 'Click to disable'
			var text = 'Active'
			var dataAction = 'disable'
		}else{
			var params = $('#adminForm').serialize()+'&task=article_unpublish&methode=ajax&cid='+cid
			var classreplace = 'btn btn-xs btn-light-grey tooltips enable publishing'
			var tooltip = 'Click to enable'
			var text = 'Disable'
			var dataAction = 'enable'
		}
		e.preventDefault()
		$.post('index.php', params, function(data){
			data = jQuery.parseJSON(data)
			if(data.type == 'success'){
				th.attr('class', classreplace)
				th.attr('data-action', dataAction)
				th.data('action', dataAction)
				th.attr('data-original-title', tooltip)
				th.text(text)
			}else{
				alert(data.message)
			}
		})
	})
	$('#adminForm').on('click', '.deleteitem', function(e){
		var th = $(this)
		var r = confirm('Delete Item?');
		var cid = th.data('id')
		if(r == true){
			var params = $('#adminForm').serialize()+'&task=article_trash&methode=ajax&cid='+cid
		}else{
			return false
		}
		//console.log(params)
		e.preventDefault()
		$.post('index.php', params, function(data){
			data = jQuery.parseJSON(data)
			if(data.type == 'success'){
				th.parent().parent().remove()
			}else{
				alert(data.message)
			}
		})
	})
});
</script>
<?php
//JHTML::_( 'behavior.mootools' );
//JHTML::_( 'behavior.modal' );
JHTML::_('jquery.datatable', '.datatable');


require_once ( JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php' );
	
$n	=	count( $this->applicants );

$javascript ='
		';
$this->document->addScriptDeclaration( $javascript );

if(JRequest::getVar('title')){
	$cat_title = ' '.JRequest::getVar('title');
}

?>

<?php if ( $this->menu_params->get( 'show_prod_search' ) ) : ?>
<?php echo $this->loadTemplate( 'searchbar' ); ?>
<?php endif; ?>
<div class="col-ms-12">
<div class="panel panel-white">
<div class="panel-heading border-light">
    <h4 class="panel-title"><?php echo $this->page_title.$cat_title; ?></h4>
    <ul class="panel-heading-tabs border-light">
        <?php echo $this->loadTemplate( 'toolbar' ); ?>
        <li class="panel-tools">
            <div class="dropdown">
                <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
                    <i class="fa fa-cog"></i>
                </a>
                <ul class="dropdown-menu dropdown-light pull-right" role="menu">
                    <li>
                        <a class="panel-refresh" href="#">
                            <i class="fa fa-refresh"></i> <span>Refresh</span>
                        </a>
                    </li>
                    <li>
                        <a class="panel-config" href="#panel-config" data-toggle="modal">
                            <i class="fa fa-wrench"></i> <span>Configurations</span>
                        </a>
                    </li>
                    <li>
                        <a class="panel-expand" href="#">
                            <i class="fa fa-expand"></i> <span>Fullscreen</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>
<div class="panel-body">
<div class="table-responsive">
<table class="table table-striped datatable border-bottom" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-color: #dfe1e5;">
<thead>
<tr>

    <th width="30" style="text-align: center;" >
		#
	</th>
   	<th class="sectiontableheader" align="left">
		Name
	</th>
    <th class="sectiontableheader" align="left">
		Job
	</th>
	<th class="sectiontableheader" align="right">
		Date
	</th>
	
</tr>
</thead>
<tbody>
<?php for ( $i = 0; $i < $n; $i++ ) {
	$row				=	$this->applicants[$i];
	
	$row->userlink		=	JURI::base().'administration/user?view=user&layout=customform&userid='.$row->user_id;

	$row->joblink		=	JURI::base().'administration/vacancy/vacancy?view=type&layout=form&typeid=22&cckid='.$row->job_id;
	
?>
<tr class="sectiontableentry<?php echo ( $i % 2 == 0 ) ? 2 : 1;?>">

	<td width="24" align="right" style="padding-right: 10px;">
		<?php echo $i + 1; ?>
	</td>
    <td align="left">
    	<a href="<?php echo $row->userlink; ?>"><strong><?php echo $row->name; ?></strong></a>
    </td>
	<td align="left">
    	<a href="<?php echo $row->joblink; ?>"><strong><?php echo $row->title; ?></strong></a>
    </td>
    
    <td width="<?php echo $this->menu_params->get( 'date_width' ); ?>" align="left">
        <?php echo JHTML::_('date',  $row->date, '%d %B %Y' ); ?>
    </td>

    
    
</tr>
<?php } ?>
</tbody>

</table>
</div>
</div>

</div>
</div>