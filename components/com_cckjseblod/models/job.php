<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			SEBLOD 1.x (CCK for Joomla!)
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

/**
 * Article		Model Class
 **/
class CCKjSeblodModelJob extends JModel
{
	/**
	 * Vars
	 **/
	var $_data		=	null;
	
	/**
	 * Constructor
	 **/
	function __construct()
	{
		parent::__construct();
		
		$user		=&	JFactory::getUser();
		
		$userId		=	$user->get('id');
		$userGId	=	$user->get('gid');
		$this->setValues( $userId, $userGId );
	}

	/**
	 * Set Values
	 **/
	function setValues( $userId, $userGId )
	{
		// Set Values
		$this->_data	=	null;
		$this->_userId	=	$userId;
		
		$this->_userGId	=	$userGId;
	}
	
	/**
	 * Get Data from Database
	 **/
	function &getData()
	{
		if ( empty( $this->_data ) )
		{
			global $mainframe;
			$params	=&	$mainframe->getParams();

			$query	= "SELECT a.* , u.name, c.title ".
				"FROM #__job_application AS a ".
				"INNER JOIN #__users AS u ON u.id = a.user_id ".
				"INNER JOIN #__content AS c ON c.id = a.job_id ".
				"ORDER BY a.date DESC ";

				$this->_db->setQuery( $query );
				$this->_data	=	$this->_db->loadObjectList();

		}
		$error = $this->_db->getErrorMsg();
		return $this->_data;
	}
	
	/**
	 * Get Data from Database
	 **/
	function _buildContentWhere()
	{
		global $mainframe;
		$params	=&	$mainframe->getParams();
				
		if ( $params->get( 'category_id', 0 ) ) {
			$catid	=	$params->get( 'category_id', 0 );
			
			//if ( $params->get( 'include_subcategories', 0 ) ) {
				// TODO check article from every child + selected
			//	$where_category	=	'';
			//} else {
				$where_category	=	' AND a.catid = '.(int)$catid;
			//}
		}elseif(JRequest::getVar('catid')){
			$catid	= JRequest::getVar('catid');
			$where_category	=	' AND a.catid = '.(int)$catid;
		}elseif( $params->get( 'secid', 0 )){
			$secid	=	$params->get( 'secid', 0 );
			$where_category	=	' AND a.sectionid = '.(int)$secid;
		}elseif(JRequest::getVar('secid')){
			$secid	=	JRequest::getVar('secid');
			$where_category	=	' AND a.sectionid = '.(int)$secid;
		}elseif( $params->get( 'content_type', 0 )){
			$typeid	=	$params->get( 'typeid', 0 );
			$where_category	=	' AND a.content_type = '.(int)$typeid;
		}elseif(JRequest::getVar('typeid')){
			$typeid	=	JRequest::getVar('typeid');
			$where_category	=	' AND a.content_type = '.(int)$typeid;
		}
			

		
		$where_state	=	'';
		$where_state	=	( $params->get( 'show_published', 1 ) ) ? 'a.state=1' : '';
		$where_state	.=	( $params->get( 'show_unpublished', 0 ) ) ? ( ( $where_state ) ? ' OR a.state=0' : 'a.state=0' ) : '';
		$where_state	.=	( $params->get( 'show_archived', 0 ) ) ? ( ( $where_state ) ? ' OR a.state=-1' : 'a.state=-1' ) : '';
		$where_state	=	( $where_state ) ? '('.$where_state.')' : 'a.state=-2';
		
		$where			=	' WHERE '.$where_state.$where_category;
		if ( ! $params->get( 'enable_all_authors', 1 ) ) {
			$where		.=	' AND a.created_by = '.$this->_userId;
		}
		if(JRequest::getVar('keyword')){
			$where		.=	" AND a.title LIKE '%".JRequest::getVar('keyword')."%'";
		}
		if(JRequest::getVar('sku')){
			$where		.=	" AND a.sku LIKE '%".JRequest::getVar('sku')."%'";
		}
		$where_excluded	=	$this->_buildContentWhereExcluded();
		if ( $where_excluded ) {
			$where .= ' AND a.id NOT IN ('.$where_excluded.')';
		}
		
		return $where;
	}
	
	/**
	 * Get Data from Database
	 **/
	function _buildContentWhereExcluded()
	{
		$query	= 'SELECT contentid FROM #__jseblod_cck_users'
				. ' WHERE registration=1 AND userid = '.(int)$this->_userId
				;
		$this->_db->setQuery( $query );
		$excluded	=	$this->_db->loadResultArray();
		
		if ( is_array( $excluded ) ) {
			$excluded	=	implode( ',', $excluded );
		}
		
		return $excluded;
	}
	
	/**
	 * Get Data from Database
	 **/
	function _buildContentOrderBy()
	{
		global $mainframe;
		$params	=&	$mainframe->getParams();
		require_once ( JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'query.php');
		
		$orderby	= ' ORDER BY ';
		$order		=	$params->get( 'orderby_sec', 'order' );
		$orderby	.=	ContentHelperQuery::orderbySecondary( $order );
		
		return $orderby;
	}
	
		
	/**
	 * Publish / Unpublish
	 **/
	function publish( $cid = array(), $publish = 1 )
	{
		global $mainframe;
		
		if ( count( $cid ) ) {
			JArrayHelper::toInteger( $cid );
			$cids	=	implode( ',', $cid );
				
			$query	= 'UPDATE #__content'
					. ' SET state = '.(int)$publish
					. ' WHERE id IN ( '.$cids.' )'
					;
			$this->_db->setQuery( $query );
			if ( ! $this->_db->query() ) {
				$this->setError( $this->_db->getErrorMsg() );
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Delete
	 **/
	function trash()
	{
		global $mainframe;
		JTable::addIncludePath( JPATH_SITE.DS.'libraries'.DS.'joomla'.DS.'database'.DS.'table' );
				
		$cids	=	JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row	=&	JTable::getInstance( 'content', 'JTable' );
		
		if ( $n = count( $cids ) )
		{
			foreach($cids as $cid) {
				if ( ! $row->delete( $cid ) ) {
					$this->setError( $this->_db->getErrorMsg() );
					return false;
				}
			}						
		}
		
		return $n;
	}
}
?>