<?php
/**
 * @version		$Id: controller.php 16385 2010-04-23 10:44:15Z ian $
 * @package		Joomla
 * @subpackage	Content
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * User Component Controller
 *
 * @package		Joomla
 * @subpackage	Weblinks
 * @since 1.5
 */
class UserController extends JController
{
	
	function display()
	{
		parent::display();
	}
	
	function storeToken(){
		
		if($media == 'fb'){
			$session->set('fbtoken', $token);
		}
	}
	
	

	function edit()
	{
		global $mainframe, $option;

		$db		=& JFactory::getDBO();
		$user	=& JFactory::getUser();

		if ( $user->get('guest')) {
			JError::raiseError( 403, JText::_('Access Forbidden') );
			return;
		}

		JRequest::setVar('layout', 'form');

		parent::display();
	}
	
	function newsletter($name = '', $email = null, $act = 'add'){
		$db = JFactory::getDBO();
		$query = "SELECT email FROM #__newsletter WHERE email = '".$email."'";
		$db->setQuery($query);
		$exist = count($db->loadObjectList());
		$secret = strtr(base64_encode($email), '+/=', '-_,');
		if($act == 'add'){
			if($email){
				if($exist == 0){
					$query = "INSERT INTO #__newsletter (email,type,secret,full_name,status) VALUES ('$email','nw','$secret','$name',1)";
				}else{
					$query = "UPDATE #__newsletter SET status = 1 WHERE email = '".$email."'";
				}
				$db->setQuery($query);
					$db->query();
			}
		}
		if($act == 'del'){
			if($email){
				if($exist){
					$query = "UPDATE #__newsletter SET status = 0 WHERE email = '".$email."'";
				}else{
					$query = "INSERT INTO #__newsletter (email,type,secret,full_name,status) VALUES ('$email','nw','$secret','$name',0)";
				}
				$db->setQuery($query);
				$db->query();
			}
		}
	}

	function storeProfilePicture($file){

	}

	function save()
	{
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );

		global $mainframe;
		
		jimport('joomla.user.helper');

		$db = &JFactory::getDBO();
		$user	 =& JFactory::getUser();
		$userid = JRequest::getVar( 'id', 0, 'post', 'int' );

		
		$id = $mainframe->getUserState($this->_namespace . 'id');
		$token = $mainframe->getUserState($this->_namespace . 'token');

		// preform security checks
		if ($user->get('id') == 0 || $userid == 0 || $userid <> $user->get('id')) {
			JError::raiseError( 403, JText::_('Access Forbidden') );
			return;
		}

		//clean request
		$post = JRequest::get( 'post' );
		$post['username']	= JRequest::getVar('username', '', 'post', 'username');
		$post['password']	= JRequest::getVar('password', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$post['password2']	= JRequest::getVar('password2', '', 'post', 'string', JREQUEST_ALLOWRAW);
	
		// get the redirect
		$return = JRequest::getVar('return', '', 'method', 'base64');
		
		if(!$return){
			$return = '/user';
		}else{
			$return = base64_decode($return);
		}
		
		
		if($post['act'] == 'pass'){
			if(!strlen($post['password'])){
				$msg	= JText::_('Fill out new password');
				$this->setRedirect($return, $msg, 'error');
				return false;
			}
		}
		
		// do a password safety check
		if(strlen($post['password']) || strlen($post['verifypassword'])) { // so that "0" can be used as password e.g.
			if($post['password'] != $post['verifypassword']) {
				$msg	= JText::_('PASSWORDS_DO_NOT_MATCH');
				// something is wrong. we are redirecting back to edit form.
				// TODO: HTTP_REFERER should be replaced with a base64 encoded form field in a later release
				
				$this->setRedirect(base64_decode($return), $msg, 'error');
				return false;
			}else{
				
				
				
				$salt		= JUserHelper::genRandomPassword(32);
				$crypt		= JUserHelper::getCryptedPassword($password1, $salt);
				$password	= $crypt.':'.$salt;
				
				$query 	= 'UPDATE #__users'
				. ' SET password = '.$db->Quote($password)
				. ' WHERE id = '.(int) $id;

				$db->setQuery($query);
		
				// Save the password
				if (!$result = $db->query())
				{
					$this->setRedirect($return, JText::_('DATABASE_ERROR'), 'error');
					return false;
				}
			}
		}

		$query = 'UPDATE #__user_profile'
			. ' SET birthdate = ' . $db->Quote(JRequest::getVar('birthdate'))
			. ' , hometown = ' . $db->Quote(JRequest::getVar('hometown'))
			. ' , gender = ' . $db->Quote(JRequest::getVar('gender'))
			. ' , bio = ' . $db->Quote(JRequest::getVar('bio'))
			. ' , job = ' . $db->Quote(JRequest::getVar('job'))
			. ' , study = ' . $db->Quote(JRequest::getVar('study'))
			. ' , degree = ' . $db->Quote(JRequest::getVar('degree'))
			. ' , class = ' . $db->Quote(JRequest::getVar('class'))
			. ' WHERE id = ' . (int) $user->get('id');

		$db->setQuery($query);

		//print_r($user->get('id'));die();
		
				// Save the password
		if (!$db->query()) {
			$this->setRedirect($return, $db->getErrorMsg(), 'error');
			return false;
		}

		// we don't want users to edit certain fields so we will unset them
		unset($post['gid']);
		unset($post['block']);
		unset($post['usertype']);
		unset($post['registerDate']);
		unset($post['activation']);
		
		$address = new stdClass();
		
		$address->address = JRequest::getVar('address');
		
		$post['address'] = json_encode($address);

		// store data
		$model = $this->getModel('user');
		
		if(!$post['newsletter']){
			$post['newsletter'] = 0;
		}

		if ($model->store($post)) {
			$this->setRedirect( $return, JText::_( 'Your settings have been saved.' ) , 'success');
		}else{
			$msg	= $model->getError();
			$this->setRedirect( $return, $msg, 'error' );
		}

		
	}

	function cancel()
	{
		$this->setRedirect( 'index.php' );
	}
	
	function login($credentials = array())
	{
		// Check for request forgeries
		//echo JUtility::getToken(); die();
		//JRequest::checkToken('request') or jexit( 'Invalid Token' );

		global $mainframe;

		if ($return = JRequest::getVar('return', '', 'method', 'base64')) {
			$return = base64_decode($return);
       
			if (!JURI::isInternal($return)) {
				$return = '';
			}
		}else if (JRequest::getVar('template') != 'frontend') {
			$return = JURI::base().'user';
		}
    
    

		$options = array();
		$options['remember'] = JRequest::getBool('remember', false);
		$options['return'] = $return;


		$credentials['username'] = JRequest::getVar('username', '', 'method', 'username');
		$credentials['password'] = JRequest::getString('passwd', '', 'post', JREQUEST_ALLOWRAW);

		//preform the login action
		$error = $mainframe->login($credentials, $options);
		if(!$error->message)
		{
			$model = $this->getModel('user');

      if(JRequest::getVar('firebase_token')){
  			$model->store(array('firebase_token' => JRequest::getVar('firebase_token')));
      }

			// Redirect if the return url is not registration or login
			$return	= JRoute::_('index.php?option=com_user');

			$mainframe->redirect( $return );

		}else{
			// Facilitate third party login forms
			if ( ! $return ) {
				$return	= JRoute::_('index.php?option=com_user&view=login');
			}
			// Redirect to a login form
			$mainframe->redirect( $return, $error->message );
		}
	}
  
	function fblogin(){
		global $mainframe;
		$model = $this->getModel('user');
		$redirect = base64_decode(JRequest::getVar('return'));

		if(JRequest::getVar('token', false)){
			$token = JRequest::getVar('token');
		}else{
			$token = JRequest::getVar('fbtoken');
		}

		$fbuser = $model->fblogin($token);

		if (!$return) {
			$return = '\user';
		}

		if ($fbuser){
			$model->store(array('firebase_token' => JRequest::getVar('firebase_token')));
			$mainframe->redirect($return, 'Welcome, ' . $fbuser->name, 'Success');
		} else {
			$mainframe->redirect($return, 'Error login you in', 'Danger');
		}
	}
	
	function gLogin(){
		$model = $this->getModel('user');
		
		$model->gLogin(JRequest::getVar('gtoken'));
	}

	function logout()
	{
		global $mainframe;

		//preform the logout action
		$error = $mainframe->logout();

		if(!JError::isError($error))
		{
			if ($return = JRequest::getVar('return', '', 'method', 'base64')) {
				$return = base64_decode($return);
				if (!JURI::isInternal($return)) {
					$return = '';
				}
			}

			// Redirect if the return url is not registration or login
			if ( $return && !( strpos( $return, 'com_user' )) ) {
				$mainframe->redirect( $return );
			}

			if(JRequest::getVar('request_type') == 'api'){
				$content = new stdClass();
				$content->display_message = 'Logout success';
				$content->message = $content->display_message;
				$content->status = 200;
				$content->debug_request = JRequest::get('default');
				echo json_encode($content);die();
			}

      $mainframe->redirect( JURI::base() );


		} else {
			parent::display();
		}
	}

	/**
	 * Prepares the registration form
	 * @return void
	 */
	function register()
	{
		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		if (!$usersConfig->get( 'allowUserRegistration' )) {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		$user 	=& JFactory::getUser();

		if ( $user->get('guest')) {
			JRequest::setVar('view', 'register');
		} else {
			$this->setredirect('index.php?option=com_user&task=edit',JText::_('You are already registered.'));
		}

		parent::display();
	}

	/**
	 * Save user registration and notify users and admins if required
	 * @return void
	 */
	 
	function impotUser($table = null)
	{
		jimport('joomla.user.helper');
		$table = JRequest::getVar('table');
		if(!$table){
			die('Define a table!');
		}
		$db = JFactory::getDBO();
		$query = "SELECT * FROM #__".$table;
		$db->setQuery($query);
		$results = $db->loadObjectList();
		
		foreach($results as $result){			
			$query = "SELECT meta_key, meta_value FROM #__wp_usermeta WHERE user_id = ".$result->ID;
			$db->setQuery($query);
			$metas = $db->loadObjectList();
			foreach($metas as $meta){	
				$meta_key = $meta->meta_key;
				$result->$meta_key = $meta->meta_value;
			}
		}
		
		//print_r($results);die();
		
		if(!$results){
			die('Table empty!');
		}
		
		$authorize	=& JFactory::getACL();
		$newUsertype = 'Registered';
		
		$reports = array();
		
		foreach($results as $result){		
			$report = new stdClass();
			$report->email = $result->user_email;
			$instance = new stdClass();	
			$instance = clone(JFactory::getUser());
			$instance->set('id', 0);
			$instance->set('usertype', $newUsertype);
			$instance->set('gid', $authorize->get_group_id( '', $newUsertype, 'ARO' ));
			$date =& JFactory::getDate();
			$instance->set('registerDate', $result->user_registered);
			$instance->set('lastvisitDate', $date->toMySQL());
			if($result->first_name){
				$instance->set( 'name'				, $result->first_name.' '.$result->last_name);
			}elseif($result->billing_first_name){
				$instance->set( 'name'				, $result->billing_first_name.' '.$result->billing_last_name);
			}elseif($result->shipping_first_name){
				$instance->set( 'name'				, $result->shipping_first_name.' '.$result->shipping_last_name);
			}else{
				$instance->set( 'name'				, $result->user_email);
			}
			$instance->set( 'phone'				, $result->billing_phone );
			$instance->set( 'newsletter'	, 1 );
			$instance->set( 'username'		, $result->user_email );
			$instance->set( 'email'				, $result->user_email );	// Result should contain an email (check)
			

			$random		= JUserHelper::genRandomPassword(5);
			
			$instance->set( 'password'		, md5($random) );
			$instance->set( 'activation'		, $random );
			$instance->set( 'fbid'		, '' );

			$address = new stdClass();
			$addr = new stdClass();
			$addr->name = $result->shipping_first_name.' '.$result->shipping_last_name;
			$addr->phone = $result->billing_phone;
			$addr->country = 999;
			$addr->country_name = 'Indonesia';
			$addr->province = $this->getLocalProvince($result->billing_city);
			$addr->district = '';
			$addr->district_name = '';
			$addr->postal = $result->shipping_postcode;
			$addr->address = $result->shipping_address_1.' '.$result->shipping_address_2;
			$address->address = $addr;
			$instance->set('address'			, json_encode($address));
			$instance->set('guest', 1);
			
			if ( !$instance->save() )
			{
				$report->status = $instance->getError();
			}else{
				$report->status = 'Success';
			}
			
			$reports[] = $report;
		}
		print_r($reports);
		die();
	}
	
	function getLocalProvince($keyword = null){
		$db = JFactory::getDBO();
		$query = "SELECT kota FROM #__jne WHERE kota LIKE '%".$keyword."%' GROUP BY kota";
		$db->setQuery($query);
		$result = $db->loadResult();
		if($db->getErrorMsg()){
			die($error);
		}
		return $result;
	}

  function validate_nim($nim = null, $name = null){
		if(!$nim){
			$this->setRedirect($this->defRegister, 'Nim dibutuhkan', 'error');
			return false;
		}
		if(!$name){
			$this->setRedirect($this->defRegister, 'Nama dibutuhkan', 'error');
			return false;
		}

		$name = strtoupper($name);

		$db = JFactory::getDBO();
		$query = "SELECT nim FROM #__data_alumni WHERE nama = '$name'";
		$db->setQuery($query);
		$result = $db->loadResult();
		if($db->getErrorMsg()){
			die($db->getErrorMsg());
		}

		if(!$result){
			$this->setRedirect('/user', 'Periksa kembali nama dan NIM anda', 'error');
			return false;
		}

		if($result != $nim ){
			$this->setRedirect('/user', 'Periksa kembali nama dan NIM anda', 'error');
			return false;
		}
		return true;
	}
	 
	function register_save()
	{
		global $mainframe;

		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );

		

		// Get required system objects
		$user 		= clone(JFactory::getUser());
		$pathway 	=& $mainframe->getPathway();
		$config		=& JFactory::getConfig();
		$authorize	=& JFactory::getACL();
		$document   =& JFactory::getDocument();

		// If user registration is not allowed, show 403 not authorized.
		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		if ($usersConfig->get('allowUserRegistration') == '0') {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		$post = JRequest::get('post');

		if((!$post['name']) || (!$post['email'])){
			$this->setRedirect('/user', 'Incomplete registration data', 'error');
			return false;
		}

		if ((!$post['password']) || (!$post['password2'])) {
			$this->setRedirect('/user', 'Set password for your account', 'error');
			return false;
		}

		if ($post['password'] != $post['password2']) {
			$this->setRedirect('/user', 'Password don\'t match' , 'error');
			return false;
		}

    if($post['npm'] != '10101010'){
      if(!$this->validate_nim($post['npm'], $post['name'])){
        return false;
      }
    }
    

		// Initialize new usertype setting
		$newUsertype = $usersConfig->get( 'new_usertype' );
		if (!$newUsertype) {
			$newUsertype = 'Registered';
		}

		// Bind the post array to the user object
		if (!$user->bind( JRequest::get('post'), 'usertype' )) {
			JError::raiseError( 500, $user->getError());
		}

		// Set some initial user values
		if(!JRequest::getVar('username')){
			$user->set('username', JRequest::getVar('email'));
		}
		$user->set('id', 0);
		$user->set('usertype', $newUsertype);
		$user->set('gid', $authorize->get_group_id( '', $newUsertype, 'ARO' ));

		$date =& JFactory::getDate();
		$user->set('registerDate', $date->toMySQL());


		// If user activation is turned on, we need to set the activation information
		$useractivation = $usersConfig->get( 'useractivation' );
		if ($useractivation == '1')
		{
			jimport('joomla.user.helper');
			$user->set('activation', JUtility::getHash( JUserHelper::genRandomPassword()) );
			$user->set('block', '1');
		}
		
		$saved = $user->save();
     
		// If there was an error with registration, set the message and display form
		if ( $saved == false )
		{
     
			$this->setRedirect('/user', $user->getError(), 'error');
			//$this->register();
			return false;
		}

		

		$db = &JFactory::getDBO();

		$userId = $user->get('id');
		$birthdate = JRequest::getVar('birthdate');
		$hometown = JRequest::getVar('hometown');
		$gender = JRequest::getVar('gender');
		$bio = JRequest::getVar('bio');
		$job = JRequest::getVar('job');
		$study = JRequest::getVar('study');
		$degree = JRequest::getVar('degree');

		$query = "INSERT INTO #__user_profile (id, hometown, birthdate, gender, bio, job, study, degree) VALUES ('$userId', '$hometown', '$birthdate','$gender','$bio','$job','$study','$degree')";

		$db->setQuery($query);

    $storeProfile = $db->query();
		
		if ($storeProfile == false) {
			$this->setRedirect('/user', $db->getErrorMsg(), 'error');
			return false;
		}
    
		// Send registration confirmation mail
		$password = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
		$passwordNochar = preg_replace('/[\x00-\x1F\x7F]/', '', $password); //Disallow control chars in the email
		//print_r($passwordNochar);die();
    $this->_sendMail($user, $passwordNochar);

    //$this->fast_request(JURI::base()."index.php?option=com_user&view=user&task=sendMailExt&user".$user->get('id')."&password=".$passwordNochar."&token=".JUtility::getToken());

		// Everything went fine, set relevant message depending upon user activation state and display message
		if ( $useractivation == 1 ) {
			$message  = JText::_( 'REG_COMPLETE_ACTIVATE' );
		} else {
			$message = 'Registration succesfull';
			$credential = array(
				'name' => $user->get('name'),
				'username' => $user->get('email'),
				'email' => $user->get('email'),
				'password' => $password,
			);
      
			$auth = $mainframe->login($credential);
      
		}
		

		$this->setRedirect('/user', $message);
	}

  function fast_request($url)
{
    $parts=parse_url($url);
    $fp = fsockopen($parts['host'],isset($parts['port'])?$parts['port']:80,$errno, $errstr, 30);
    $out = "GET ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Length: 0"."\r\n";
    $out.= "Connection: Close\r\n\r\n";

    fwrite($fp, $out);
    fclose($fp);
}

	function activate()
	{
		global $mainframe;

		// Initialize some variables
		$db			=& JFactory::getDBO();
		$user 		=& JFactory::getUser();
		$document   =& JFactory::getDocument();
		$pathway 	=& $mainframe->getPathWay();

		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		$userActivation			= $usersConfig->get('useractivation');
		$allowUserRegistration	= $usersConfig->get('allowUserRegistration');

		// Check to see if they're logged in, because they don't need activating!
		if ($user->get('id')) {
			// They're already logged in, so redirect them to the home page
			$mainframe->redirect( 'index.php' );
		}

		if ($allowUserRegistration == '0' || $userActivation == '0') {
			JError::raiseError( 403, JText::_( 'Access Forbidden' ));
			return;
		}

		// create the view
		require_once (JPATH_COMPONENT.DS.'views'.DS.'register'.DS.'view.html.php');
		$view = new UserViewRegister();

		$message = new stdClass();

		// Do we even have an activation string?
		$activation = JRequest::getVar('activation', '', '', 'alnum' );
		$activation = $db->getEscaped( $activation );

		if (empty( $activation ))
		{
			// Page Title
			$document->setTitle( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ) );
			// Breadcrumb
			$pathway->addItem( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ));

			$message->title = JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' );
			$message->text = JText::_( 'REG_ACTIVATE_NOT_FOUND' );
			$view->assign('message', $message);
			$view->display('message');
			return;
		}

		// Lets activate this user
		jimport('joomla.user.helper');
		if (JUserHelper::activateUser($activation))
		{
			// Page Title
			$document->setTitle( JText::_( 'REG_ACTIVATE_COMPLETE_TITLE' ) );
			// Breadcrumb
			$pathway->addItem( JText::_( 'REG_ACTIVATE_COMPLETE_TITLE' ));

			$message->title = JText::_( 'REG_ACTIVATE_COMPLETE_TITLE' );
			$message->text = JText::_( 'REG_ACTIVATE_COMPLETE' );
		}
		else
		{
			// Page Title
			$document->setTitle( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ) );
			// Breadcrumb
			$pathway->addItem( JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' ));

			$message->title = JText::_( 'REG_ACTIVATE_NOT_FOUND_TITLE' );
			$message->text = JText::_( 'REG_ACTIVATE_NOT_FOUND' );
		}

		$view->assign('message', $message);
		$view->display('message');
	}

	/**
	 * Password Reset Request Method
	 *
	 * @access	public
	 */
	function requestreset()
	{
		global $mainframe;
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$email		= JRequest::getVar('email', null, 'post', 'string');

		// Get the model
		$model = &$this->getModel('Reset');

		// Request a reset
		if ($model->requestReset($email) === false)
		{
			$message = JText::sprintf('PASSWORD_RESET_REQUEST_FAILED', $model->getError());
			$this->setRedirect(JURI::base().'user', $message, 'error');
			return false;
		}
		$mainframe->setUserState('com_user.reset.reqemail',	$email);
    if(JRequest::getVar('template') == frontend){
      $this->setRedirect(JURI::base().'login/reset?layout=confirm', 'Request reset success', 'info');
    }else{
      $this->setRedirect(JURI::base().'user', 'Request reset success', 'info');
    }
		
	}

	/**
	 * Password Reset Confirmation Method
	 *
	 * @access	public
	 */
	function confirmreset()
	{
		global $mainframe;
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$token = JRequest::getVar('token', null, 'post', 'alnum');
		
		if(trim(JRequest::getVar('email'))){
			$email = JRequest::getVar('email');
		}else{
			$email = $mainframe->getUserState('com_user.reset.reqemail');
		}
				

		// Get the model
		$model = &$this->getModel('Reset');

		// Verify the token
		if ($model->confirmReset($token, $email) !== true)
		{
			$message = JText::sprintf('PASSWORD_RESET_CONFIRMATION_FAILED', $model->getError());
      if(JRequest::getVar('template') == frontend){
      $this->setRedirect(JURI::base().'login/reset?layout=confirm', $message, 'error');
        }else{
          $this->setRedirect(JURI::base().'user', $message, 'error');
        }
		}
		$this->setRedirect(JURI::base().'user', 'Reset token success', 'info');
	}

	/**
	 * Password Reset Completion Method
	 *
	 * @access	public
	 */
	function completereset()
	{
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$password1 = JRequest::getVar('password1', null, 'post', 'string', JREQUEST_ALLOWRAW);
		$password2 = JRequest::getVar('password2', null, 'post', 'string', JREQUEST_ALLOWRAW);

		// Get the model
		$model = &$this->getModel('Reset');

		// Reset the password
		if ($model->completeReset($password1, $password2) === false)
		{
			$message = JText::sprintf('PASSWORD_RESET_FAILED', $model->getError());
			$this->setRedirect('/user', $message, 'error');
			return false;
		}

		$this->setRedirect('/user', 'Password reset success', 'info');
	}

	/**
	 * Username Reminder Method
	 *
	 * @access	public
	 */
	function remindusername()
	{
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );

		// Get the input
		$email = JRequest::getVar('email', null, 'post', 'string');

		// Get the model
		$model = &$this->getModel('Remind');

		// Send the reminder
		if ($model->remindUsername($email) === false)
		{
			$message = JText::sprintf('USERNAME_REMINDER_FAILED', $model->getError());
			$this->setRedirect('index.php?option=com_user&view=remind', $message);
			return false;
		}

		$message = JText::sprintf('USERNAME_REMINDER_SUCCESS', $email);
		$this->setRedirect('index.php?option=com_user&view=login', $message);
	}

  function sendMailExt(){
    $user = JRequest::getVar('user');
    $password = JRequest::getVar('password');
    $token = JRequest::getVar('token');
    $token2 = JUtility::getToken();
    //if($token == $token2){
      $user = JFactory::getUser($user);
      $this->_sendMail($user, $password);
    //}
    return;
  }

	function _sendMail($user, $password)
	{
		global $mainframe;

		$db		=& JFactory::getDBO();

		$name 		= $user->get('name');
		$email 		= $user->get('email');
		$username 	= $user->get('username');

		$usersConfig 	= &JComponentHelper::getParams( 'com_users' );
		$sitename 		= $mainframe->getCfg( 'sitename' );
		$useractivation = $usersConfig->get( 'useractivation' );
		$mailfrom 		= $mainframe->getCfg( 'mailfrom' );
		$fromname 		= $mainframe->getCfg( 'fromname' );
		$siteURL		= JURI::base();

		$subject 	= sprintf ( JText::_( 'Account details for' ), $name, $sitename);
		$subject 	= html_entity_decode($subject, ENT_QUOTES);

		if ( $useractivation == 1 ){
			$message = sprintf ( JText::_( 'SEND_MSG_ACTIVATE' ), $name, $sitename, $siteURL."index.php?option=com_user&task=activate&activation=".$user->get('activation'), $siteURL, $username, $password);
		} else {
			$message = 'Thank you for registering.';
		}

		$message = html_entity_decode($message, ENT_QUOTES);

		//get all super administrator
		$query = 'SELECT name, email, sendEmail' .
				' FROM #__users' .
				' WHERE LOWER( usertype ) = "super administrator"';
		$db->setQuery( $query );
		$rows = $db->loadObjectList();

		// Send email to user
		if ( ! $mailfrom  || ! $fromname ) {
			$fromname = $rows[0]->name;
			$mailfrom = $rows[0]->email;
		}

		JUtility::sendMail($mailfrom, $fromname, $email, $subject, $message);

    return;

		// Send notification to all administrators
		$subject2 = sprintf ( JText::_( 'Account details for' ), $name, $sitename);
		$subject2 = html_entity_decode($subject2, ENT_QUOTES);

		// get superadministrators id
		foreach ( $rows as $row )
		{
			if ($row->sendEmail)
			{
				$message2 = sprintf ( JText::_( 'SEND_MSG_ADMIN' ), $row->name, $sitename, $name, $email, $username);
				$message2 = html_entity_decode($message2, ENT_QUOTES);
				//JUtility::sendMail($mailfrom, $fromname, $row->email, $subject2, $message2);
			}
		}
	}

	function propict(){
		global $mainframe;
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		$uploaded = $this->upload();
		$query = 'UPDATE #__user_profile'
			. ' SET propict = ' . $db->Quote($uploaded)
			. ' WHERE id = ' . (int) $user->get('id');

		$db->setQuery($query);

		if(!$db->query()) {
			$this->setRedirect('/user', $db->getErrorMsg(), 'error');
			return false;
		}

		$user->set('propict', $uploaded);
		$session = &JFactory::getSession();
		$session->set('user', $user);

		// check if username has been changed
		if ($username != $user->get('username')) {
			$table = JUser::getTable('session', 'JTable');
			$table->load($session->getId());
			$table->propict = $user->get('propict');
			$table->store();

		}



		//die('Profile picture saved');
		$mainframe->redirect('/user');
		//$this->setRedirect('/user', 'Profile picture saved', 'info');
	}

	function upload(){
		jimport('joomla.filesystem.file');
		$result = new stdClass();
		$user = JFactory::getUser();
		$file = JRequest::getVar('file','', FILES);
		$filename = $file['name'];
		$cleanFilename = JFile::makeSafe($filename);
		$shorthpath = 'userfile'.DS.$user->get('id').DS.$cleanFilename;
		$fullpath = JPATH_BASE.DS.$shorthpath;
		if (!JFile::upload($file['tmp_name'], $fullpath)){
			die('error moving file');
		}
		return $shorthpath;
	}


	function addExperience(){
		global $mainframe;
		$db = &JFactory::getDBO();
		$user = JFactory::getUser();

		$user_id = $user->get('id');
		$company_name = JRequest::getVar('company_name');
		$position = JRequest::getVar('position');
		$start_date = JRequest::getVar('start_date');
		$end_date = JRequest::getVar('end_date');
		$job_description = JRequest::getVar('job_description');

		$query = "INSERT INTO #__user_experience (user_id, company_name, position, start_date, end_date, job_description) ".
						 " VALUES ('$user_id', '$company_name', '$position','$start_date','$end_date','$job_description')";

		$db->setQuery($query);

		if (!$db->query()) {
			$mainframe->redirect('/user', $db->getErrorMsg(), 'error');
		}
		$mainframe->redirect('/user');
	}
}
?>
