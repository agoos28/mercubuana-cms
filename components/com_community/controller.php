<?php
/**
 * @version		$Id: controller.php 16385 2010-04-23 10:44:15Z ian $
 * @package		Joomla
 * @subpackage	Content
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * User Component Controller
 *
 * @package		Joomla
 * @subpackage	Weblinks
 * @since 1.5
 */
class CommunityController extends JController
{
	
	function display()
	{
		parent::display();
	}
	
	function getList(){
		$model = &$this->getModel('People');
		$view = & $this->getView('people', 'html');
		$view->setModel($model, true);
		$view->setLayout('default');
		$view->display();
	}

	function getPeople(){
		$model = &$this->getModel('People');
    $view = & $this->getView('people', 'html');
		$view->setModel($model, true);
		$view->setLayout('detail');
		$view->display();
		//echo json_encode($model->getPeople());
	}

	function friendship(){
		$targetId = JRequest::getVar('id');
		$code = JRequest::getVar('code');
		$model = &$this->getModel('People');
		echo json_encode($model->friendship($targetId, $code));
	}

	function getFriends(){
		$model = &$this->getModel('People');
		echo json_encode($model->getPeople());
	}

	function getMessages()
	{
		$model = &$this->getModel('Message');
		echo json_encode($model->getList());
	}

	function sendMessage()
	{
		$model = &$this->getModel('Message');
		echo json_encode($model->send());
	
	}

	function deleteMessage()
	{
		$model = &$this->getModel('Message');
		echo json_encode($model->delete_messages());
	}

	function jobApply()
	{
		$job_id = JRequest::getVar('id');
		$model = &$this->getModel('Company');
		echo json_encode($model->apply_job($job_id));
	}
}
?>
