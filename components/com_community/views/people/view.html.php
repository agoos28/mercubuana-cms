<?php
/**
* @version		$Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @subpackage	Registration
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Registration component
 *
 * @package		Joomla
 * @subpackage	Registration
 * @since 1.0
 */
class communityViewPeople extends JView
{
	function display($tpl = null)
	{
		
		global $mainframe;
    //echo json_encode($model->getList());
    $model = $this->getModel();

    $limitstart	= JRequest::getVar('start', 0);
    $limit = $mainframe->getUserStateFromRequest('com_community.'.$this->getLayout().'.limit', 'limit', 12);

    //print_r($model);die();
    $list = $model->getList();
    $total = $model->getTotal();

    jimport('joomla.html.pagination');
		//In case we are in a blog view set the limit
    
		$pagination = new JPagination($total, $limitstart, $limit);

    $this->assign('total', $total);
    $this->assign('list',	$list);
    $this->assign('pagination',	$pagination);
    //print_r($this);die();
		parent::display($tpl);
	}
}
