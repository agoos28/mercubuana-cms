<?php // @version $Id: default.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');

$num_columns = 1;
$num_intro = 12;

$i = 0;
$total = $this->pagination->total;
?>
<div class="wrapper">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="page-title-box">
          <div class="btn-group pull-right">
            <ol class="breadcrumb hide-phone p-0 m-0">
              <li class="breadcrumb-item"><a href="members.html">PUBLIC</a></li>
              <li class="breadcrumb-item"><a href="members-pending.html">PENDING</a></li>
              <li class="breadcrumb-item"><a href="members-friend.html">FRIENDS</a></li>
            </ol>
          </div>
          <h4 class="page-title">People</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <form class="form-inline">
          <div class="form-group m-r-10">
            <input type="text" class="form-control" id="exampleInputName2" placeholder="Find a Friend">
          </div>
          <button type="submit" class="btn btn-primary waves-effect waves-light btn-md">
            SEARCH
          </button>
        </form>
      </div><!-- end col -->
      <div class="col-sm-8">
        <div class="text-right">
          <?php echo $this->pagination->getPagesLinks(); ?>
        
      </div>
    </div>
    <div class="row">

      <?php for ($z = 0; $i < $total && $z < ($num_intro / $num_columns); $z++) {
        $rows = (int)($num_intro / $num_columns);
        $cols = ($num_intro % $num_columns);
        $loop = (($z < $cols) ? 1 : 0) + $rows;

        ?>

        <?php for ($y = 0; $y < $rows; $y++) {
          if ($i < $total && $y < ($num_columns)) {
            $item = $this->list[$i];
            ?>
            <div class="col-md-4 col-sm-6">
              <div class="text-center card-box">
                <div class="dropdown pull-right">
                  <a href="#" class="dropdown-toggle card-drop arrow-none" data-toggle="dropdown" aria-expanded="false">
                    <h3 class="m-0 text-muted"><i class="mdi mdi-dots-horizontal"></i></h3>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="#">Edit</a>
                    <a class="dropdown-item" href="#">Delete</a>
                    <a class="dropdown-item" href="#">Block</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="member-card">
                  <div class="thumb-xl member-thumb m-b-10 mx-auto">
                    <img style="width: 120px; height: 120px;" src="https://web.mercubuana.msolving.space/<?php echo $item->propict; ?>" class="rounded-circle img-thumbnail" alt="profile-image">
                    <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>
                  </div>

                  <div class="">
                    <h4 class="m-b-5"><?php echo $item->name; ?></h4>
                    <p class="text-muted" ><?php echo $item->class; ?> | <?php echo $item->study; ?></p>
                  </div>

                  <p class="text-muted font-13" style="height: 50px; overflow: hidden;">
                    <?php echo $item->bio; ?>
                  </p>

                  <button type="button" class="btn btn-primary m-t-20 btn-rounded btn-bordered waves-effect w-md waves-light">Add as Friend</button>

                </div>

              </div>

            </div>
            <?php $i++;
          }
        }
        ?>
      <?php
    }
    ?>
    </div>
  </div>
</div>