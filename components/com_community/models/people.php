<?php
/**
 * @version		$Id: reset.php 21046 2011-03-31 16:11:40Z dextercowley $
 * @package		Joomla
 * @subpackage	User
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * User Component Reset Model
 *
 * @package		Joomla
 * @subpackage	User
 * @since		1.5
 */
class communityModelPeople extends JModel
{
	/**
	 * Registry namespace prefix
	 *
	 * @var	string
	 */
	
	var $_namespace	= 'com_community.people.';
  var $_total = null;
	var $_user = null;

	function __construct()
	{
		parent::__construct();
		$this->setValues();
	}

	function setValues()
	{
		// Set Values
		$this->_data = null;
		$this->_where = null;
		$this->_loggedUser = JFactory::getUser();
	}

	

	function getPeople(){
		if(!$this->_data){
			$this->_buildWhere();
			$this->_buildQuery();
		}
		return $this->_data[id];
	}

	

	function filter(){}

	function _buildWhere(){
		$userid = $this->_loggedUser->get('id');
		$storekey = JRequest::getVar('storekey');
		$filter = JRequest::getVar('filter', false);

		if($filter){
			$this->_where .= " AND  a.name LIKE '%".$filter."%'";
		}
		
		if($storekey == 'friends'){
			$this->_where .= " AND c.addressee_id = ".$userid. " AND (c.status = 'A' OR c.status = 'R')";
		}

		if($storekey == 'persons' && $filter == false){
			//$this->_where .= " AND  c.status IS NULL OR c.status != 'A'";
		}
		
	}

  

	function _buildQuery()
	{
		$userid = $this->_loggedUser->get('id');
		$query = "SELECT a.id, a.name, b.study, b.class, b.job, b.hometown, b.bio, b.propict, c.status AS friendship_status, c.specifier_Id "
			. " FROM #__users AS a"
			. " LEFT JOIN #__user_profile AS b ON b.id = a.id"
			. " LEFT JOIN #__user_friendship AS c "
			. " ON (c.requester_id = a.id AND c.addressee_Id = $userid)"
			. " OR (c.requester_id = $userid AND c.addressee_Id = a.id)"
			. " WHERE a.id != ".$userid . $this->_where . " ORDER BY a.name ASC";

		return $query;
	}

  function getList($params = null){

      $limit		= JRequest::getVar('limit', 12);
			$limitstart	= (int)JRequest::getVar('start', 0);
			
			if($limit == 0){
				$limit = $limitstart;
			}
			$this->_buildWhere();
			$query = $this->_buildQuery();

      $this->_db->setQuery( $query, $limitstart, $limit );
      $this->_data = $this->_db->loadObjectList();
      if ($err = $this->_db->getErrorMsg()) {
        die($err);
      }else{
        foreach($this->_data as $item){
          $item->friends = $this->addPersonFriends($item->id);
          $item->experience = $this->addUserExperience($item->id);
        }

        return $this->_data;
      }

		return $this->_data;
	}

  function _getListCount( $query ){
		$this->_db->setQuery( $query );
		$this->_db->query();
		return $this->_db->getNumRows();
	}

  function getTotal($state = 1)
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_total)){
			$query = $this->_buildQuery($state);
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function get_person($id)
	{
		$userid = $this->_loggedUser->get('id');
		$query = "SELECT a.id, a.name, a.firebase_token, b.study, b.class, b.job, b.hometown, b.bio, b.propict, c.status AS friendship_status, c.specifier_Id "
			. " FROM #__users AS a"
			. " LEFT JOIN #__user_profile AS b ON b.id = a.id"
			. " LEFT JOIN #__user_friendship AS c "
			. " ON (c.requester_id = a.id AND c.addressee_Id = $userid)"
			. " OR (c.requester_id = $userid AND c.addressee_Id = a.id)"
			. " WHERE a.id = " . $id;

		$this->_db->setQuery($query);

		$person = $this->_db->loadObject();
		if ($err = $this->_db->getErrorMsg()) {
			die($err);
			return false;
		} else {

			$person->friends = $this->addPersonFriends($person->id);
			$person->experience = $this->addUserExperience($person->id);

			return $person;
		}
	}

  function checkFriendsRecord($requester_id, $addressee_Id){
		$query = "SELECT * FROM #__user_friendship WHERE requester_id = ".$requester_id." AND addressee_Id = ".$addressee_Id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();
		if($error = $this->_db->getErrorMsg()){
			return false;
		}
		return $result;
	}

  function friendship($targetId, $friendship_status = 'R'){

		global $mainframe;

		$userid = $this->_loggedUser->get('id');
		
		if($userid < $targetId){
			$requester_id = $userid;
			$addressee_Id = $targetId;
		}else{
			$requester_id = $targetId;
			$addressee_Id = $userid;
		}
		$result = $this->checkFriendsRecord($requester_id, $addressee_Id);

		
		if((!$result->friendship_id) && $friendship_status == 'R'){
			$query = "INSERT INTO #__user_friendship (requester_id, addressee_Id, submit_date, status, specifier_Id) VALUES ('$requester_id', '$addressee_Id', NOW(), '$friendship_status', '$userid')";
		}else{
			if($friendship_status == 'R'){
				//accepted
				if($result->specifier_Id != $userid && $result->status == 'R'){
					$query = "UPDATE #__user_friendship SET submit_date = NOW(), status = 'A', specifier_Id = '$userid' WHERE friendship_id = ".$result->friendship_id;
				}
			}
			if ($friendship_status == 'U' || $friendship_status == 'C') {
				//Unfriend
				
					$query = "DELETE FROM #__user_friendship WHERE friendship_id = " . $result->friendship_id;

			}
			if($friendship_status == 'A'){
				//accepted
				if($result->status == 'R' && $result->specifier_Id != $userid){
					$query = "UPDATE #__user_friendship SET submit_date = NOW(), status = 'A', specifier_Id = '$userid' WHERE friendship_id = ".$result->friendship_id;
				}
			}
			if($friendship_status == 'D'){
				if($targetId == $userid){
					$query = "DELETE FROM #__user_friendship WHERE friendship_id = ".$result->friendship_id;
				}
			}
			if($friendship_status == 'B'){
				
				if($userid == $result->specifier_Id && $result->status == 'B'){
						$query = "DELETE FROM #__user_friendship WHERE friendship_id = ".$result->friendship_id;
				}
				if($result->status != 'B'){
						$query = "UPDATE #__user_friendship SET submit_date = NOW(), status = 'B', specifier_Id = '$userid' WHERE friendship_id = ".$result->friendship_id;
				}
			}
			
		}
		$this->_db->setQuery($query);
		$this->_db->query();
		if ($err = $this->_db->getErrorMsg()) {
			die($err);
			return false;
		}else{
			$ret = new stdClass();
			$data = new stdClass();
			$ret->id = $targetId;
			$ret->friendship_status = $friendship_status;
			$person = $this->get_person($userid);
			$target = $this->get_person($targetId);
			$person->screen = 'UserDetail';
			$data->person = $person;
			$data->messageData = array();
			$data->messageData[0] = new stdClass();
			$data->messageData[0]->name = $person->name;
			$data->messageData[0]->user_id = 'fr_'.$person->id;
			$data->screen = "userDetail";
			$data->user_id = $person->id;
			$data->thumbnail = $person->propict;
			if($friendship_status == 'R' || $friendship_status == 'A'){
				if($friendship_status == 'A'){
					$data->messageData[0]->message = 'Menerima pertemanan anda';
				}else{
					$data->messageData[0]->message = 'Ingin menjalin pertemanan';
				}
				$this->sendPushNotification(
					$target->firebase_token,
					(array) $data
				);
			}
			return $target;
		}
	}


  

  

	function addUserExperience($id){
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__user_experience WHERE user_id = " . $id . " ORDER BY start_date DESC";
		$db->setQuery($query);
		$experience = $db->loadObjectList();
		return $experience;
	}


	function addPersonFriends($id){
		$query = "SELECT a.friendship_id, b.id, b.name, c.study, c.degree, c.class, c.propict FROM #__user_friendship AS a LEFT JOIN #__users AS b ON b.id = a.requester_id LEFT JOIN #__user_profile AS c ON c.id = b.id  WHERE a.addressee_Id =".$id . " LIMIT 6";
		$this->_db->setQuery($query);
		$one = $this->_db->loadObjectList();

		$query = "SELECT a.friendship_id, b.id, b.name, c.study, c.degree, c.class, c.propict FROM #__user_friendship AS a LEFT JOIN #__users AS b ON b.id = a.addressee_Id LEFT JOIN #__user_profile AS c ON c.id = b.id  WHERE a.requester_id =".$id." LIMIT 6";
		$this->_db->setQuery($query);
		$two = $this->_db->loadObjectList();

		$friends = (object) array_merge((array) $one, (array) $two);

		if ($err = $this->_db->getErrorMsg()) {
			die($err);
		}else{
			return $friends;
		}
	}

  function sendPushNotification($to, $data = array())
	{
		$fields = array(
			'to' => $to,
			'tag' => $this->_loggedUser->get('id'),
			'collapse_key' => 'fr',
			'priority' => 'high',
			'data' => $data,
			//'notification' => $notification,
		);

		$url = 'https://fcm.googleapis.com/fcm/send';

		$headers = array(
			'Authorization: key=' . 'AIzaSyDuu7aSWkqOhi2Q8j1KjjJO-7i9fWl9aLc',
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		curl_setopt($ch, CURLOPT_VERBOSE, true);

		$result = curl_exec($ch);

		if ($result === false) {
			die('Curl failed: ' . curl_error($ch));
		}
		curl_close($ch);
		return $result;
	}
}
