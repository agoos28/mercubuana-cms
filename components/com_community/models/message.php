<?php
/**
 * @version		$Id: reset.php 21046 2011-03-31 16:11:40Z dextercowley $
 * @package		Joomla
 * @subpackage	User
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * User Component Reset Model
 *
 * @package		Joomla
 * @subpackage	User
 * @since		1.5
 */
class communityModelMessage extends JModel
{
	/**
	 * Registry namespace prefix
	 *
	 * @var	string
	 */
	
	var $_namespace	= 'com_community.message.';

	var $_user = null;

	function __construct()
	{
		parent::__construct();
		$this->setValues();
	}

	function setValues()
	{
		// Set Values
		$this->_data = null;
		$this->_where = null;
		$this->_loggedUser = JFactory::getUser();
	}

	function getList($params = null){
		if(!$this->_data){
			$this->_buildWhere();
			$this->_buildQuery();
		}
		return $this->_data;
	}

	function send(){
		if(!$this->_loggedUser->id){
			JError::raiseError(401, 'Unauthorized');
		}
		global $mainframe;
		
	// Check for request forgeries
		//JRequest::checkToken() or jexit('Invalid Token');

		require_once(JPATH_ADMINISTRATOR . DS . 'components'.DS.'com_messages' . DS . 'tables' . DS . 'message.php');

		$db = &JFactory::getDBO();
		$row = new TableMessage($db);

		if (!$row->bind(JRequest::get('post'))) {
			JError::raiseError(500, $row->getError());
		}

		$row->set('user_id_from', $this->_loggedUser->id);

		if (!$row->check()) {
			print_r($row);die();
			JError::raiseError(500, $row->getError());
		}

		if (!$row->send()) {
			print_r($row);die();
			JError::raiseError(500, $row->getError());
		}

		$result = $this->get_message($row->message_id);
		$messageData = $this->get_message_for_notif($row->message_id);

		$pushNotif = $this->sendPushNotification(
			$result[0]->user_id, 
			array(
				'title' => $this->_loggedUser->name, 
				'body' => $result[0]->message
			), 
			array(
				"user_id" => $this->_loggedUser->id,
				"name" => $this->_loggedUser->name,
				"thumbnail" => $this->_loggedUser->propict,
				"messageData" => (array)$messageData,
				"screen" => "MessageDetail"
			)
		);
		
		return $result;
	}


	function filter(){
		
			
	}

	function _buildWhere(){
		$userid = $this->_loggedUser->get('id');
		$filter = JRequest::getVar('filter');
	}

	function getUserFirebaseToken($userid = 0){
		$query = "SELECT firebase_token FROM #__users WHERE id = ".$userid;
		$this->_db->setQuery($query);
		return $this->_db->loadResult();
	}

	function sendPushNotification($to, $message, $data = array())
	{
		$to = $this->getUserFirebaseToken($to);
		$notification = array(
			'title' => $message['title'],
			'body' => $message['body'],
			'sound' => 'default'
		);

		$fields = array(
			'to' => $to,
			'tag' => $data['user_id'],
			'collapse_key' => 'msg',
			'priority' => 'high',
			'data' => $data,
			//'notification' => $notification,
		);
         
		$url = 'https://fcm.googleapis.com/fcm/send';
 
		$headers = array(
			'Authorization: key=' . 'AIzaSyDuu7aSWkqOhi2Q8j1KjjJO-7i9fWl9aLc',
			'Content-Type: application/json'
		);
 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		curl_setopt($ch, CURLOPT_VERBOSE, true);
 
		$result = curl_exec($ch);

		if ($result === false) {
			die('Curl failed: ' . curl_error($ch));
		}
		curl_close($ch);
		return $result;
	}

	function get_message($id){
		$userid = $this->_loggedUser->get('id');
		$query = "SELECT a.*, unix_timestamp(a.date_time) AS date_time, b.name, b.id AS 'user_id', c.propict AS 'thumbnail'"
			. " FROM #__messages AS a"
			. " LEFT JOIN #__users AS b ON ((b.id = a.user_id_from AND b.id != $userid) OR (b.id = a.user_id_to AND b.id != $userid))"
			. " LEFT JOIN #__user_profile AS c ON c.id = b.id"
			. " WHERE a.message_id = ".$id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function get_message_for_notif($id){
		$userid = $this->_loggedUser->get('id');
		$query = "SELECT a.*, unix_timestamp(a.date_time) AS date_time, b.name, b.id AS 'user_id', c.propict AS 'thumbnail'"
			. " FROM #__messages AS a"
			. " LEFT JOIN #__users AS b ON b.id = ".$userid
			. " LEFT JOIN #__user_profile AS c ON c.id = ".$userid
			. " WHERE a.message_id = ".$id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
	function delete_messages(){
		$uid = JRequest::getVar('uid');
		$userid = $this->_loggedUser->get('id');
		$query = "DELETE FROM #__messages WHERE ((user_id_from = $uid AND user_id_to = $userid) OR (user_id_to = $uid AND user_id_from = $userid))";
		$this->_db->setQuery($query);
		$this->_db->query();
		return;
	}

	function _buildQuery()
	{
		global $mainframe;
		$base = JURI::base();
		$userid = $this->_loggedUser->get('id');
		$uid = JRequest::getVar('uid');
		$lastMessageId = JRequest::getVar('lastMessageId', 0);
		
		if($uid){
			$where = " WHERE ((a.user_id_from = $uid AND a.user_id_to = $userid) OR (a.user_id_to = $uid AND a.user_id_from = $userid))";
			$group = "";
		}else{
			$where = " WHERE ((a.user_id_from = $userid OR a.user_id_to = $userid))";
			$group = " GROUP BY user_id";
		}
		$where .= " AND a.message_id > ". (int)$lastMessageId;

		$query = "SELECT a.*, unix_timestamp(a.date_time) AS date_time, b.name, b.id AS 'user_id', c.propict AS 'thumbnail'"
			. " FROM #__messages AS a"
			. " LEFT JOIN #__users AS b ON ((b.id = a.user_id_from AND b.id != $userid) OR (b.id = a.user_id_to AND b.id != $userid))"
			. " LEFT JOIN #__user_profile AS c ON c.id = b.id"
			. $where
			//. $group
			. " ORDER BY a.date_time DESC LIMIT 100";

		$this->_db->setQuery($query);
		$this->_data = $this->_db->loadObjectList();
		if ($err = $this->_db->getErrorMsg()) {
			die($err);
			return false;
		}else{
			return $this->_data;
		}
	}
}
