<?php
/**
 * @version		$Id: remind.php 14401 2010-01-26 14:10:00Z louis $
 * @package		Joomla
 * @subpackage	User
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * User Component Remind Model
 *
 * @package		Joomla
 * @subpackage	User
 * @since		1.5
 */
class communityModelCompany extends JModel
{
	/**
	 * Registry namespace prefix
	 *
	 * @var	string
	 */
	var $_namespace	= 'com_community.company.';

	function getAdminMail(){
		$query = 'SELECT name, email, sendEmail' .
				' FROM #__users' .
				' WHERE LOWER( usertype ) = "super administrator" AND sendEmail = 1';
		$this->_db->setQuery( $query );
		$rows = $this->_db->loadObjectList();
		return $rows;
	}

	function validate($job_id){
		$user = JFactory::getUser();
		$user_id = $user->id;

		$query = "SELECT _id FROM #__job_application ".
		" WHERE job_id = '$job_id' AND user_id = '$user_id'";
		$this->_db->setQuery($query);
		$found = $this->_db->loadResult();

		if($found){
			die('Anda sudah pernah mendaftar');
		}
	}

	function apply_job($job_id){
		$this->validate($job_id);

		$user = JFactory::getUser();
		$user_id = $user->id;
		
		$job = $this->getContent($job_id);
		$adminMails = $this->getAdminMail();
		$subject = 'New job application for '.$job->title. ' from '.$user->name;
		$message = 'New job application for '.$job->title. ' from '.$user->name;

		$query = "INSERT INTO #__job_application (job_id, user_id) VALUES ('$job_id', '$user_id')";
		$this->_db->setQuery($query);
		if(!$this->_db->query()){
			die('Terjadi kesalahan, coba ulang lagi');
		}
		for($i=0; $i < count((array)$adminMails); $i++){
			$this->_sendMail($adminMails[$i]->email, $subject, $message);
		}
		die('Sukses, terima kasih');
	}

	function getContent($id){
		$query = 'SELECT a.*,' .
				' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
				' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'.
				' FROM #__content AS a' .
				' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
				' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
				' WHERE a.state = 1 ' .
				//' AND (a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' ) ' .
				//' AND (a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )' .
				' AND a.id = '. (int) $id ;	
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();
		$result->raw = $this->getRawContent($result->introtext);
		return $result;
	}

	/**
	 * Sends a username reminder to the e-mail address
	 * specified containing the specified username.
	 *
	 * @since	1.5
	 * @param	string	A user's e-mail address
	 * @param	string	A user's username
	 * @return	bool	True on success/false on failure
	 */
	function _sendMail($to, $subject, $message)
	{
		$config		= &JFactory::getConfig();
		$uri		= &JFactory::getURI();

		$from		= $config->getValue('mailfrom');
		$fromname	= $config->getValue('fromname');

		if (!JUtility::sendMail($from, $fromname, $to, $subject, $message))
		{
			$this->setError('ERROR_SENDING_EMAIL');
			return false;
		}

		return true;
	}

	function getRawContent($text = null)
    {	
		$res	=	array();
		$group	=	array();
		$regex	=	'#::(.*?)::(.*?)::/(.*?)::#s';
		$i = 0;
		preg_match_all( $regex, $text, $matches );
		
		if ( sizeof( $matches[1] ) ) {
			foreach ( $matches[1] as $key => $val ) {
				$gmatch = explode('|', $val);
				if(count($gmatch) > 1){
					$gkey = $gmatch[1];
					$iname = $gmatch[0];
					$gname = $gmatch[2];
					$group[$gkey][$iname] = $matches[2][$key];
					$res[$gname] = $group;
				}else{
					$res[$val]	=	$matches[2][$key];
				}
			}
		}
		return (object)$res;
    }
}